<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    
    public function sign_payment_request($secret_key, &$params) {
        $fields_for_sign = array('mid', 'order_id', 'payment_type',
                                'amount', 'ccy');

        if (isset($params['payer_id'])) {
            $fields_for_sign[] = 'payer_id';
        }

        $aggregated_field_str = "";
        foreach ($fields_for_sign as $f) {
            $aggregated_field_str .= trim($params[$f]);
        }

        if ($params['api_mode'] == 'direct_n3d'
            || $params['api_mode'] == 'redirection_n3d') {
            $pan = '';
            if (isset($params['card_no'])) {
                    $pan = trim($params['card_no']);
            }
            else if (isset($params['token_id'])) {
                    $pan = trim($params['token_id']);
            }
            $first_6 = substr($pan, 0, 6);
            $last_4 = substr($pan, -4);
            $aggregated_field_str .= $first_6 . $last_4;
            if (isset($params['exp_date'])) {
                    $aggregated_field_str .=
                        trim($params['exp_date']);
            }

            if (isset($params['cvv2'])) {
                    $cvv2 = trim($params['cvv2']);
                    $last_digit_cvv2 = substr($cvv2, -1);
                    $aggregated_field_str .= $last_digit_cvv2;     
            }
        }
        $aggregated_field_str .= $secret_key;
        $signature = hash('sha512', $aggregated_field_str);
        return $signature;
    }

    // Store Contact Form data
    public function ContactUsForm(Request $request) {

        // Red dot payment things
        $secret_key = "D716A4188569B68AB1B6DFAC178E570114CDF0EA3A1CC0E31486C3E41241BC6A76424E8C37AB26F096FC85EF9886C8CB634187F4FDDFF645FB099F1FF54C6B8C";

        $request_transaction = array(
            "merchant_reference" => "testing",
            "payer_name" => "andreas",
            "card_no" => "4111111111111111",
            "exp_date" => "112017",
            "cvv2" => "123",
            "mid" => "1000089029",
            "order_id" => "TST101",
            "amount" => "1.02",
            "ccy" => "SGD",
            "api_mode" => "direct_n3d",
            "payment_type" => "S",
            "payer_email" => "andreas@example.com"
        );

        $signature = $this->sign_payment_request($secret_key, $request_transaction);
        $request_transaction['signature'] = $signature;
        $json_request = json_encode($request_transaction);

        $response = $this->post($json_request);
        error_log($response);
        //$response_array = json_decode($response, true);
        //error_log($response_array);
        return back()->with('success', $response);
    }

    public function post($json_request) {
        $url = "https://secure-dev.reddotpayment.com/service/payment-api";
        $curl = curl_init($url);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POST => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POSTFIELDS => $json_request,
            CURLOPT_HTTPHEADER =>
                array('Content-Type: application/json')));
        $response = curl_exec($curl);
        if ($response == null) {
            error_log("this is null");
        }
        $curl_errno = curl_errno($curl);
        $curl_err = curl_error($curl);
        error_log($curl_err);
        curl_close($curl);
        return $response;
    }
}
