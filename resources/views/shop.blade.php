@extends('layouts.app')

@section('content')
<div class="container d-flex">
    <div class="col-3">
        <div class="Filters & stuff">
            <p>Filters & stuff</p>
        </div>
    </div>
    <div class="col-12">
        <div class="container">
        <h1>Clothes</h1>
        <div class="row d-flex">
            <div class="col-4">
                <div class="card" style="height:100%;">
                    <img src="/image/redshirt.jpeg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Red shirt</h5>
                        <p class="card-text">A college athlete who is withdrawn from university sporting events for a year to develop their skills and extend their period of playing eligibility by a further year at this level of competition.</p>
                        <a href="#" class="btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" style="height:100%;">
                    <img src="/image/blueshirt.jpeg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Blue shirt</h5>
                        <p class="card-text">Barba long sleeve shirt</p>
                        <a href="#" class="btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card" style="height:100%;">
                    <img src="/image/greenshirt.jpeg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Green shirt</h5>
                        <p class="card-text">From paisley and patterned to Oxford and Gingham, our iconic collection of Pretty Green shirts nod to our love of British tailoring with a modern twist.</p>
                        <a href="#" class="btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
